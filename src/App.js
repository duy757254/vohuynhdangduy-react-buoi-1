import logo from "./logo.svg";
import "./App.css";
import Header from "./BaiTapLayOutComponent/Header";
import Body from "./BaiTapLayOutComponent/Body";
import Items from "./BaiTapLayOutComponent/Items";
import Footer from "./BaiTapLayOutComponent/Footer";

function App() {
  return (
    <div className="App ">
      <Header />
      <Body />
      <Items />
      <Footer />
    </div>
  );
}

export default App;
