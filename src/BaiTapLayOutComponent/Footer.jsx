import React, { Component } from "react";

export default class Footer extends Component {
  render() {
    return (
      <footer className="bg-dark">
        <div className="py-5 ">
          <div className="container">
            <p className=" text-center text-white">
              Copyright © Your Website 2022
            </p>
          </div>
        </div>
      </footer>
    );
  }
}
