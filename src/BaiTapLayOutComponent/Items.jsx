import React, { Component } from "react";
import "./Style.css";

export default class Items extends Component {
  render() {
    return (
      <div className="Items mt-5">
        <div className="container">
          <div className="row">
            {/* <div className="card-item">
              <div className="card-icon"></div>
              <div className="card-text">
                <h3>Fresh new layout</h3>
                <p>
                  With Bootstrap 5, we've created a fresh new layout for this
                  template!
                </p>
              </div>
            </div> */}
            <div className="col-4 mb-5">
              <div className="card bg-light border-0 h-100">
                <div className="card-body text-center  ">
                  <div className="feature bg-primary bg-gradient text-white rounded  mb-4 mt-n4">
                    <i className="fa fa-cloud-download-alt" />
                  </div>
                  <h2 className=" fw-bold">Fresh new layout</h2>
                  <p className="mb-0">
                    With Bootstrap 5, we've created a fresh new layout for this
                    template!
                  </p>
                </div>
              </div>
            </div>
            <div className="col-4 mb-5">
              <div className="card bg-light border-0 h-100">
                <div className="card-body text-center  ">
                  <div className="feature bg-primary bg-gradient text-white rounded  mb-4 mt-n4">
                    <i className="fab fa-npm" />
                  </div>
                  <h2 className=" fw-bold">Fresh new layout</h2>
                  <p className="mb-0">
                    With Bootstrap 5, we've created a fresh new layout for this
                    template!
                  </p>
                </div>
              </div>
            </div>
            <div className="col-4 mb-5">
              <div className="card bg-light border-0 h-100">
                <div className="card-body text-center  ">
                  <div className="feature bg-primary bg-gradient text-white rounded  mb-4 mt-n4">
                    <i className="fab fa-app-store-ios" />
                  </div>
                  <h2 className=" fw-bold">Fresh new layout</h2>
                  <p className="mb-0">
                    With Bootstrap 5, we've created a fresh new layout for this
                    template!
                  </p>
                </div>
              </div>
            </div>
            <div className="col-4 mb-5">
              <div className="card bg-light border-0 h-100">
                <div className="card-body text-center  ">
                  <div className="feature bg-primary bg-gradient text-white rounded  mb-4 mt-n4">
                    <i className="fa fa-dog" />
                  </div>
                  <h2 className=" fw-bold">Fresh new layout</h2>
                  <p className="mb-0">
                    With Bootstrap 5, we've created a fresh new layout for this
                    template!
                  </p>
                </div>
              </div>
            </div>
            <div className="col-4 mb-5">
              <div className="card bg-light border-0 h-100">
                <div className="card-body text-center  ">
                  <div className="feature bg-primary bg-gradient text-white rounded  mb-4 mt-n4">
                    <i className="fab fa-css3" />
                  </div>
                  <h2 className=" fw-bold">Fresh new layout</h2>
                  <p className="mb-0">
                    With Bootstrap 5, we've created a fresh new layout for this
                    template!
                  </p>
                </div>
              </div>
            </div>
            <div className="col-4 mb-5">
              <div className="card bg-light border-0 h-100">
                <div className="card-body text-center  ">
                  <div className="feature bg-primary bg-gradient text-white rounded  mb-4 mt-n4">
                    <i className="fa fa-eye" />
                  </div>
                  <h2 className=" fw-bold">Fresh new layout</h2>
                  <p className="mb-0">
                    With Bootstrap 5, we've created a fresh new layout for this
                    template!
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
