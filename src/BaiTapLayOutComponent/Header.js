import React, { Component } from "react";

export default class Header extends Component {
  render() {
    return (
      <div className=" header px-lg-5 bg-dark">
        <nav className="navbar container navbar-expand-lg  navbar-dark ">
          <div>
            <a className="navbar-brand  " href="#">
              Start Bootstrap
            </a>
          </div>
          <button
            className="ml-auto navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#collapsibleNavbar"
          >
            <span className="navbar-toggler-icon " />
          </button>
          <div className="collapse navbar-collapse" id="collapsibleNavbar">
            <ul className="text-white navbar-nav ml-auto">
              <li className=" active nav-item">
                <a className=" font-weight-normal nav-link " href="#">
                  Home
                </a>
              </li>
              <li className="nav-item align-center">
                <a className="nav-link" href="#">
                  About
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">
                  Contact
                </a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    );
  }
}
