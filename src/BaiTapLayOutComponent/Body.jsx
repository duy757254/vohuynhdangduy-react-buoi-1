import React, { Component } from "react";

export default class Body extends Component {
  render() {
    return (
      <div className="py-5 body">
        <div className=" p-5 p-lg-5 container text-center  bg-light">
          <h1 className="display-4 pt-5  font-weight-bold">A warm welcome!</h1>
          <h4 className="font-weight-normal">
            Bootstrap utility classes are used to create this jumbotron since
            the old component has been removed from the framework. Why create
            custom CSS when you can use utilities?
          </h4>
          <button className="btn mb-5  btn-primary btn-lg my-3">
            Call to action
          </button>
        </div>
      </div>
    );
  }
}
